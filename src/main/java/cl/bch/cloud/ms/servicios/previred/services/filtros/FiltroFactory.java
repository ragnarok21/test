package cl.bch.cloud.ms.servicios.previred.services.filtros;


import cl.bch.cloud.ms.servicios.previred.annotation.FiltroStrategy;
import cl.bch.cloud.ms.servicios.previred.dtos.PreviredDTO;
import cl.bch.cloud.ms.servicios.previred.enums.FiltroEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.core.annotation.AnnotationUtils.findAnnotation;

@Component
public class FiltroFactory {

    @Autowired
    private ApplicationContext applicationContext;

    private Map<FiltroEnum, FiltroService> filtroServiceMap = new HashMap<>();

    @PostConstruct
    private void init() {

        applicationContext.getBeansWithAnnotation(FiltroStrategy.class)
                .forEach((s, o) -> {
                    FiltroStrategy anotacion = findAnnotation(o.getClass(), FiltroStrategy.class);
                    filtroServiceMap.put(anotacion.tipoFiltro(), (FiltroService) o);
                });
    }

    public void obtenerFiltro(FiltroEnum filtroEnum, PreviredDTO entradaPrevired) {

        FiltroService filtroService = filtroServiceMap.get(filtroEnum);
        Assert.notNull(filtroService, "Obtencion de filtro : '" + filtroEnum.name() + "', no encontrado");
        filtroService.applyFilter(entradaPrevired);
    }

}
