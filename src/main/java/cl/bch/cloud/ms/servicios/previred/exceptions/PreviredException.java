package cl.bch.cloud.ms.servicios.previred.exceptions;

public class PreviredException extends RuntimeException {

    private static final long serialVersionUID = -6335680575056700227L;

    public PreviredException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public PreviredException(final String message) {
        super(message);
    }
}
