package cl.bch.cloud.ms.servicios.previred.dtos;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Data;

@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@Data
@AllArgsConstructor
public class ClienteCompraPreviredResponse {
    
    private String codigo;
    private String xml;
    
    
}
