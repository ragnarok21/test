package cl.bch.cloud.ms.servicios.previred.services.filtros;

import cl.bch.cloud.ms.servicios.previred.annotation.FiltroStrategy;
import cl.bch.cloud.ms.servicios.previred.dtos.PreviredDTO;
import cl.bch.cloud.ms.servicios.previred.enums.CodigosCatalogoEnum;
import cl.bch.cloud.ms.servicios.previred.enums.FiltroEnum;
import cl.bch.cloud.ms.servicios.previred.exceptions.PreviredException;
import cl.bch.cloud.ms.servicios.previred.properties.PreviredProperties;
import cl.bch.cloud.ms.servicios.previred.repositories.GetFolioDao;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
@FiltroStrategy(tipoFiltro = FiltroEnum.FILTRO_LLAVE_COMPRA)
public class FiltroLlaveCompraStrategy implements FiltroService {

    private final Logger LOGGER = LoggerFactory.getLogger(FiltroLlaveCompraStrategy.class);

    private static final String EMPRESA_BCH = "BCH";
    private static final String SEPARADOR = "-";

    @Autowired
    private PreviredProperties previredProperties;


    @Autowired
    private GetFolioDao getFolioDao;

    @Override
    public void applyFilter(PreviredDTO previredDTO) {

        
        if (!EMPRESA_BCH.equalsIgnoreCase(previredDTO.getInformacionConsumidor().getEmpresa())) {
            LOGGER.info("Se usa llave definida por canales");
            String llaveReq = previredDTO.getInformacionConsumidor().getLlave();
            validarLlaveVacia(llaveReq);
        } else {
            LOGGER.info("Se genera llave para bch");
            previredDTO.getInformacionConsumidor()
                    .setLlave(generateKey(previredDTO));
        }
    }

    private void validarLlaveVacia(String llave){
        if(StringUtils.isBlank(llave)){
            LOGGER.error("Llave TOC o EFX vacía");
            throw new PreviredException(CodigosCatalogoEnum.APP_LLAVE_COMPRA_OBLIGATORIA.getMensaje());
        }
    }

    private String generateKey(PreviredDTO previredDTO) {

        BigDecimal folio = getFolioDao.getFolio();
        previredDTO.setIdCompra(folio);
        
      return new StringBuilder()
              .append(previredProperties.getPrefijo())
              .append(SEPARADOR)
              .append(previredProperties.getNroCliente())
              .append(SEPARADOR)
              //largo de 13 + id de bd con relleenos de 0 a la izquierda
              .append(StringUtils.leftPad(folio.toString(), 37, "0"))
              .toString();
    }
    
}
