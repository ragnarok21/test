package cl.bch.cloud.ms.servicios.previred.repositories.impl;

import cl.bch.cloud.ms.servicios.previred.enums.CodigosCatalogoEnum;
import cl.bch.cloud.ms.servicios.previred.exceptions.PreviredException;
import cl.bch.cloud.ms.servicios.previred.repositories.ConsultaRegistroPorFechaDao;
import cl.bch.cloud.ms.servicios.previred.repositories.core.Function;
import oracle.jdbc.OracleTypes;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.Date;

@Repository
public class ConsultaRegistroPorFechaDaoImpl extends Function implements ConsultaRegistroPorFechaDao {

    @Override
    public String name() {
        return "EXISTE_REGISTRO";
    }

    @Override
    public SqlParameter[] parameters() {

        return new SqlParameter[]{
                new SqlParameter("IN_CLIENTE_RUT", OracleTypes.NUMBER),
                new SqlParameter("IN_FECHA_INICIAL", OracleTypes.DATE),
                new SqlParameter("IN_FECHA_FINAL", OracleTypes.DATE)
        };
    }

    @Override
    public boolean existeRegistro(Integer rut, Date fechaInicio, Date fechaFin) {

        try {
            MapSqlParameterSource in =
                    new MapSqlParameterSource()
                            .addValue("IN_CLIENTE_RUT", rut)
                            .addValue("IN_FECHA_INICIAL", fechaInicio)
                            .addValue("IN_FECHA_FINAL", fechaFin);

            return super.execute(BigDecimal.class, in).intValue() > 0;
        } catch (DataAccessException dae) {
            throw new PreviredException(CodigosCatalogoEnum.APP_ERROR_GENERAL.getCodigo(), dae);
        }
    }
}