package cl.bch.cloud.ms.servicios.previred.repositories.impl;

import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import cl.bch.cloud.ms.servicios.previred.enums.CodigosCatalogoEnum;
import cl.bch.cloud.ms.servicios.previred.exceptions.PreviredException;
import cl.bch.cloud.ms.servicios.previred.repositories.ConsultaRegistroUltimosMesesDao;
import cl.bch.cloud.ms.servicios.previred.repositories.core.StoreProcedure;
import oracle.jdbc.OracleTypes;

@Repository
public class ConsultaRegistroUltimosMesesDaoImpl extends StoreProcedure implements ConsultaRegistroUltimosMesesDao {

	private static final String RUT = "IN_CLIENTE_RUT";
	private static final String DETALLE = "OUT_DETALLE";

	@Override
	protected String name() {
		return "GET_ULTIMO_REGISTRO";
	}

	@Override
	protected SqlParameter[] parameters() {
		return new SqlParameter[] { new SqlParameter(RUT, OracleTypes.NUMBER),
				new SqlOutParameter(DETALLE, OracleTypes.VARCHAR) };
	}

	@Override
	public Map<String, Object> obtenerUltimoRegistro(String rut) {

		try {
			return this.execute(new MapSqlParameterSource().addValue(RUT, rut));
		} catch (DataAccessException dae) {
			throw new PreviredException(CodigosCatalogoEnum.APP_ERROR_GENERAL.getCodigo(), dae);
		}
	}

}