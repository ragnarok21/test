package cl.bch.cloud.ms.servicios.previred.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.util.HashMap;
import java.util.Map;

@AllArgsConstructor
public enum CodigosCatalogoEnum {

    /* ERROR PERMISOS DE AUTORIZACION */
    
    PERMISO_USUARIO_NO_EXISTE(
            "SPAUTH-0001",
            "Usuario inexistente",
            HttpStatus.UNAUTHORIZED),

    PERMISO_USUARIO_NO_VALIDO(
            "SPAUTH-0002",
            "Usuario inexistente",
            HttpStatus.UNAUTHORIZED),

    PERMISO_USUARIO_NO_ACTIVO(
            "SPAUTH-0003",
            "Usuario bloqueado",
            HttpStatus.UNAUTHORIZED),

    PERMISO_EMPRESA_NO_ACTIVA(
            "SPAUTH-0004",
            "Empresa no activa",
            HttpStatus.UNAUTHORIZED),

    PERMISO_CANAL_NO_ACTIVO(
            "SPAUTH-0005",
            "Canal no activo",
            HttpStatus.UNAUTHORIZED),

    PERMISO_FORZAR_COMPRA_NO_PERMITIDA(
            "SPAUTH-0006",
            "Acción no permitida para el usuario",
            HttpStatus.UNAUTHORIZED),

    PERMISO_MAXIMO_CONSULTAS_PERMITIDAS(
            "SPAUTH-0007",
            "Máximo de consultas superadas",
            HttpStatus.UNAUTHORIZED),
    
    
    
    /* ERRORES DE APLICATIVO */
    
    APP_ERROR_GENERAL(
            "SPCOMP-0001",
            "Error de Sistema. Este servicio está temporalmente no disponible, por favor intenta más tarde.",
            HttpStatus.INTERNAL_SERVER_ERROR),

    APP_DATOS_FALTANTES(
            "SPCOMP-0002",
            "Campos obligatorios necesarios para operar",
            HttpStatus.BAD_REQUEST),
    
    APP_LLAVE_COMPRA_OBLIGATORIA(
            "SPCOMP-0003",
            "Llave de compra obligatoria",
            HttpStatus.BAD_REQUEST),

    APP_COMPRA_EXISTENTE(
            "SPCOMP-0004",
            "Importante: ya se encuentra una compra vigente",
            HttpStatus.ACCEPTED),

    APP_CLIENTE_PENSIONADO(
            "SPCOMP-0005",
            "Posible cliente sin cotizaciones, no se efectua compra",
            HttpStatus.ACCEPTED),

    APP_GUARDAR_XML(
            "SPCOMP-0006",
            "Error de Sistema. Este servicio está temporalmente no disponible, por favor intenta más tarde.",
            HttpStatus.INTERNAL_SERVER_ERROR),
    

    /* ERROR DE SERVICIO PREVIRED */

    PREVIRED_WS_ERROR_DINAMICO(
            "SPCLIE-0001",
            "Error servicio de previred, código de error {CODIGO_ERROR}",
            HttpStatus.INTERNAL_SERVER_ERROR),

    PREVIRED_WS_LLAVE_NO_EXISTE(
            "SPCLIE-0002",
            "Error servicio previred, la llave entregada no existe",
            HttpStatus.INTERNAL_SERVER_ERROR),

    PREVIRED_WS_LLAVE_NO_VALIDA(
            "SPCLIE-0003",
            "Error servicio previred, la llave entregada no es válida",
            HttpStatus.INTERNAL_SERVER_ERROR),

    PREVIRED_WS_MANDATO_UTILIZADO(
            "SPCLIE-0004",
            "Error servicio previred, mandato ya utilizado",
            HttpStatus.INTERNAL_SERVER_ERROR),

    PREVIRED_WS_SIN_INFORMACION_MOMENTANEA(
            "SPCLIE-0005",
            "Error servicio previred, en estos momentos no podemos obtener información del Rut",
            HttpStatus.INTERNAL_SERVER_ERROR),

    PREVIRED_WS_PETICION_FUERA_HORARIO(
            "SPCLIE-0006",
            "Error servicio previred, petición fuera de horario",
            HttpStatus.INTERNAL_SERVER_ERROR),
    
    PREVIRED_WS_CODIGO_AUT_INVALIDO(
            "SPCLIE-0007",
            "Error servicio previred, código de autorización inválido",
            HttpStatus.INTERNAL_SERVER_ERROR),

    PREVIRED_WS_CODIGO_AUT_INCORRECTO(
            "SPCLIE-0008",
            "Error servicio previred, código autorización especial incorrecto",
            HttpStatus.INTERNAL_SERVER_ERROR);
    



    /* Metodos para obtener Enum dado el codigo */
    private static Map<String, CodigosCatalogoEnum> map = new HashMap<>();
    @Getter
    private final String codigo;
    @Getter
    @Setter
    private String mensaje;
    @Getter
    private HttpStatus httpStatus;

    static {
        for (CodigosCatalogoEnum codigosCatalogo : CodigosCatalogoEnum.values()) {
            map.put(codigosCatalogo.codigo, codigosCatalogo);
        }
    }

    public static CodigosCatalogoEnum getCodigoValeOf(String codigo) {
        return map.get(codigo);
    }
}
