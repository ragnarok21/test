package cl.bch.cloud.ms.servicios.previred.repositories.impl;

import cl.bch.cloud.ms.servicios.previred.dtos.ClienteCompraPreviredResponse;
import cl.bch.cloud.ms.servicios.previred.dtos.PreviredDTO;
import cl.bch.cloud.ms.servicios.previred.enums.CodigosCatalogoEnum;
import cl.bch.cloud.ms.servicios.previred.exceptions.PreviredException;
import cl.bch.cloud.ms.servicios.previred.repositories.GuardarXmlPreviredDao;
import cl.bch.cloud.ms.servicios.previred.repositories.core.StoreProcedure;
import cl.bch.cloud.ms.servicios.previred.utils.CleanUtils;
import oracle.jdbc.OracleTypes;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.sql.Timestamp;

@Repository
public class GuardarXmlPreviredDaoImpl extends StoreProcedure implements GuardarXmlPreviredDao {

    private static final String SAVE_IN_TIMESTAMP = "IN_TIMESTAMP";
    private static final String CAMPO_CODIGO_ERROR = "OUT_COD_ERROR";
    private static final String CAMPO_MENSAJE_ERROR = "OUT_MENSAJE";
    private static final String SAVE_IN_XML = "IN_XML";

    @Override
    public String name() {
        return "ADD_XML";
    }

    @Override
    public SqlParameter[] parameters() {

        return new SqlParameter[]{
                new SqlParameter("IN_CODIGO", OracleTypes.NUMBER),
                new SqlParameter("IN_FOLIO", OracleTypes.NUMBER),
                new SqlParameter("IN_CLIENTE_RUT", OracleTypes.NUMBER),
                new SqlParameter("IN_USUARIO", OracleTypes.VARCHAR),
                new SqlParameter(SAVE_IN_TIMESTAMP, OracleTypes.TIMESTAMP),
                new SqlParameter(SAVE_IN_XML, OracleTypes.CLOB),
                new SqlOutParameter(CAMPO_CODIGO_ERROR, OracleTypes.NUMBER),
                new SqlOutParameter(CAMPO_MENSAJE_ERROR, OracleTypes.VARCHAR)
        };
    }

    public void saveXML(PreviredDTO previredDTO, Timestamp timestamp, 
                        ClienteCompraPreviredResponse clienteCompraPreviredResponse) {

        try {
            MapSqlParameterSource in = new MapSqlParameterSource()
                    .addValue("IN_CODIGO", clienteCompraPreviredResponse.getCodigo())
                    .addValue("IN_FOLIO", previredDTO.getIdCompra())
                    .addValue("IN_CLIENTE_RUT", 
                            CleanUtils.cleanRutSinGuionPuntosDV(previredDTO.getRutClienteCompra()))
                    .addValue("IN_USUARIO", previredDTO.getInformacionConsumidor().getNombre())
                    .addValue(SAVE_IN_TIMESTAMP, timestamp)
                    .addValue(SAVE_IN_XML, clienteCompraPreviredResponse.getXml());

            BigDecimal resp = (BigDecimal) this.execute(in).get(CAMPO_CODIGO_ERROR);
            if (resp.intValue() != 9000) {
                throw new PreviredException(CodigosCatalogoEnum.APP_GUARDAR_XML.getCodigo());
            }
        } catch (DataAccessException dae) {
            throw new PreviredException(CodigosCatalogoEnum.APP_ERROR_GENERAL.getCodigo(), dae);
        }
    }
}
