package cl.bch.cloud.ms.servicios.previred.repositories;

import java.math.BigDecimal;

public interface GetFolioDao {

    BigDecimal getFolio();
}
