package cl.bch.cloud.ms.servicios.previred.repositories;

import java.util.Date;

public interface ConsultaRegistroPorFechaDao {

    boolean existeRegistro(Integer rut, Date fechaInicio, Date fechaFin);

}