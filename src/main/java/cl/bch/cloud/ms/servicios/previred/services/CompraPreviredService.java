package cl.bch.cloud.ms.servicios.previred.services;


import cl.bch.cloud.ms.servicios.previred.dtos.PreviredDTO;

public interface CompraPreviredService {

    void comprarRenta(Boolean tipoCompra, PreviredDTO previredRequest);
}
