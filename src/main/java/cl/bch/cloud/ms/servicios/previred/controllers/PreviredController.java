package cl.bch.cloud.ms.servicios.previred.controllers;

import cl.bch.cloud.ms.servicios.previred.dtos.PreviredDTO;
import cl.bch.cloud.ms.servicios.previred.services.CompraPreviredService;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;


@RestController
@RequestMapping(value = "/v1/servicios/previred", consumes = MediaType.APPLICATION_JSON_VALUE)
public class PreviredController {

    private final Logger LOGGER = LoggerFactory.getLogger(PreviredController.class);
    
    @Autowired
    private CompraPreviredService compraPreviredService;
    

    /**
     * Implementación que permite realizar la compra de cotizaciones al previred
     *
     * @param forzarCompra    flag que permite forzar la compra pasando por menos filtros de validacion
     *                        que la compra normal
     * @param previredRequest parametros necesarios para poder realizar la
     *                        compra a los sistemas expuestos por previred
     */
    @PostMapping(path = "/compra-cotizaciones")
    @ApiOperation("Realiza la compra de cotizaciones")
    @ResponseStatus(HttpStatus.OK)
    public void obtenerCotizacionesCliente(@RequestParam(value = "forzar") Boolean forzarCompra,
                                           @Valid @RequestBody PreviredDTO previredRequest) {

        LOGGER.info("Llamando al servicio rest compra-cotizaciones");
        compraPreviredService.comprarRenta(forzarCompra, previredRequest);
    }


}