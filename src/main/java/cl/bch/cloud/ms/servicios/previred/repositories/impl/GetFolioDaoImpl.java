package cl.bch.cloud.ms.servicios.previred.repositories.impl;

import cl.bch.cloud.ms.servicios.previred.repositories.GetFolioDao;
import cl.bch.cloud.ms.servicios.previred.repositories.core.StoreProcedure;
import oracle.jdbc.OracleTypes;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;

@Repository
public class GetFolioDaoImpl extends StoreProcedure implements GetFolioDao {

    @Override
    public String name() {
        return "GET_FOLIO";
    }

    @Override
    public SqlParameter[] parameters() {

        return new SqlParameter[]{
                new SqlOutParameter("OUT_FOLIO", OracleTypes.NUMBER)};
    }

    @Override
    public BigDecimal getFolio() {

        return (BigDecimal) this.execute()
                .get("OUT_FOLIO");
    }
}
