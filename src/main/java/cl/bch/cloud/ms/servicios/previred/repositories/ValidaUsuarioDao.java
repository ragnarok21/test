package cl.bch.cloud.ms.servicios.previred.repositories;

import cl.bch.cloud.ms.servicios.previred.dtos.PreviredDTO;

import java.util.Map;

public interface ValidaUsuarioDao {

    Map<String, Object> validaUsuario(PreviredDTO entradaPrevired);

}
