package cl.bch.cloud.ms.servicios.previred.services;

import java.sql.Timestamp;

public interface PdfService {
    void firmarDocumentoToc(Timestamp timestamp);
}
