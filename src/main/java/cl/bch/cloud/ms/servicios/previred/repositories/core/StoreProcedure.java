package cl.bch.cloud.ms.servicios.previred.repositories.core;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import javax.annotation.PostConstruct;
import java.util.Map;

public abstract class StoreProcedure extends Dao {

    @PostConstruct
    public void init() {

        setSimpleJdbcCall(new SimpleJdbcCall(getDataSource())
                .withProcedureName(this.name())
                .withCatalogName(this.catalogName())
                .declareParameters(parameters()));
    }

    public Map<String, Object> execute(MapSqlParameterSource in) {

        return this.getSimpleJdbcCall().execute(in);
    }

    public Map<String, Object> execute() {

        return this.getSimpleJdbcCall().execute();
    }
}
