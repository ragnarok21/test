package cl.bch.cloud.ms.servicios.previred.repositories.impl;

import cl.bch.cloud.ms.servicios.previred.dtos.PreviredDTO;
import cl.bch.cloud.ms.servicios.previred.enums.CodigosCatalogoEnum;
import cl.bch.cloud.ms.servicios.previred.exceptions.PreviredException;
import cl.bch.cloud.ms.servicios.previred.repositories.ValidaUsuarioDao;
import cl.bch.cloud.ms.servicios.previred.repositories.core.StoreProcedure;
import oracle.jdbc.OracleTypes;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Repository
public class ValidaUsuarioDaoImpl extends StoreProcedure implements ValidaUsuarioDao {

    private static final String CANAL = "IN_CANAL";
    private static final String USUARIO = "IN_USUARIO";
    private static final String REGISTRO = "IN_REGISTRO";
    private static final String FORZAR = "IN_FORZAR";
    private static final String CODIGO_MENSAJE = "OUT_COD_ERROR";
    private static final String GLOSA_MENSAJE = "OUT_MENSAJE";

    public String name() {
        return "SP_VALIDA_USUARIO";
    }

    @Override
    public SqlParameter[] parameters() {

        return new SqlParameter[]{
                new SqlParameter(CANAL, OracleTypes.VARCHAR),
                new SqlParameter(USUARIO, OracleTypes.VARCHAR),
                new SqlParameter(REGISTRO, OracleTypes.VARCHAR),
                new SqlParameter(FORZAR, OracleTypes.NUMBER),
                new SqlOutParameter(CODIGO_MENSAJE, OracleTypes.NUMBER),
                new SqlOutParameter(GLOSA_MENSAJE, OracleTypes.VARCHAR)
        };
    }

    @Override
    public Map<String, Object> validaUsuario(PreviredDTO entradaPrevired) {

        try {
            return this.execute(
                    new MapSqlParameterSource()
                            .addValue(CANAL, entradaPrevired.getInformacionConsumidor().getCanal())
                            .addValue(USUARIO, entradaPrevired.getInformacionConsumidor().getNombre())
                            .addValue(REGISTRO, entradaPrevired.getInformacionConsumidor().getRegistro())
                            .addValue(FORZAR, entradaPrevired.getForzar() ? 1 : 0));
        } catch (DataAccessException dae) {
            throw new PreviredException(CodigosCatalogoEnum.APP_ERROR_GENERAL.getCodigo(), dae);
        }
    }
}