package cl.bch.cloud.ms.servicios.previred.dtos;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@Data
public class ConsumidorDTO {

    @NotEmpty
    private String nombre;
    @NotEmpty
    private String canal;
    @NotEmpty
    private String registro;
    @NotNull
    private String llave;
    private String tipoServicio;
    private String empresa;
    private String usuarioPrevired;
    private String passwordPrevired;

}
