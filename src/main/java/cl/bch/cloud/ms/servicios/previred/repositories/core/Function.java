package cl.bch.cloud.ms.servicios.previred.repositories.core;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import javax.annotation.PostConstruct;

public abstract class Function extends Dao {

    @PostConstruct
    public void init() {

        setSimpleJdbcCall(new SimpleJdbcCall(getDataSource())
                .withFunctionName(name())
                .withCatalogName(this.catalogName())
                .declareParameters(parameters()));
    }

    public <T> T execute(Class<T> aType, MapSqlParameterSource in) {

        return this.getSimpleJdbcCall().executeFunction(aType, in);
    }

    public <T> T execute(Class<T> aType) {

        return this.getSimpleJdbcCall().executeFunction(aType);
    }
}
