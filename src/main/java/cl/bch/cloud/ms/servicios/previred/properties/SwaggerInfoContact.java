package cl.bch.cloud.ms.servicios.previred.properties;

import lombok.Data;

@Data
public class SwaggerInfoContact {
    private String name;
    private String mail;
}