package cl.bch.cloud.ms.servicios.previred.restClients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;


@FeignClient(name = "PensionadoClient", url = "${validar.pensionado.endpoint}")
public interface PensionadoApiClient {

    @GetMapping(value = "/validar/{rutCliente}", produces = "application/json")
    Boolean esPensionado(@PathVariable("rutCliente") String rutCliente);

}