package cl.bch.cloud.ms.servicios.previred.enums;

public enum FiltroEnum {

    FILTRO_COMPRA_PERMITIDA,
    FILTRO_COMPRA_DIARIA,
    FILTRO_COMPRA_FECHAS,
    FILTRO_COMPRA_PENSIONADO,
    FILTRO_LLAVE_COMPRA
    
}
