package cl.bch.cloud.ms.servicios.previred.services.filtros;


import cl.bch.cloud.ms.servicios.previred.annotation.FiltroStrategy;
import cl.bch.cloud.ms.servicios.previred.dtos.PreviredDTO;
import cl.bch.cloud.ms.servicios.previred.enums.CodigosCatalogoEnum;
import cl.bch.cloud.ms.servicios.previred.enums.FiltroEnum;
import cl.bch.cloud.ms.servicios.previred.exceptions.PreviredException;
import cl.bch.cloud.ms.servicios.previred.repositories.ConsultaRegistroPorFechaDao;
import cl.bch.cloud.ms.servicios.previred.utils.CleanUtils;
import cl.bch.cloud.ms.servicios.previred.utils.FechaUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Date;

@Service
@FiltroStrategy(tipoFiltro = FiltroEnum.FILTRO_COMPRA_DIARIA)
public class FiltroCompraDiariaStrategy implements FiltroService {

	private static final Logger LOGGER = LoggerFactory.getLogger(FiltroCompraDiariaStrategy.class);

	@Autowired
	private ConsultaRegistroPorFechaDao consultaRegistroPorFechaDao;

    @Autowired
    private FechaUtils fechaUtils;

    @Override
    public void applyFilter(PreviredDTO previredDTO) {

        Date fechaConsulta = fechaUtils.localDateToDate(fechaUtils.getDateNow());

		if (consultaRegistroPorFechaDao.existeRegistro(
                Integer.parseInt(CleanUtils.cleanRutSinGuionPuntosDV(previredDTO.getRutClienteCompra())),
				fechaConsulta, fechaConsulta)) {
			LOGGER.error("cliente ya realizo compra");
			throw new PreviredException(CodigosCatalogoEnum.APP_COMPRA_EXISTENTE.getCodigo(),
					new Exception("cliente ya realizo compra"));
		}
		
	}
}
