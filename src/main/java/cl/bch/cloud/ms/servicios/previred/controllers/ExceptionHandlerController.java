package cl.bch.cloud.ms.servicios.previred.controllers;

import cl.bch.cloud.ms.servicios.previred.dtos.ErrorInfoDTO;
import cl.bch.cloud.ms.servicios.previred.enums.CodigosCatalogoEnum;
import cl.bch.cloud.ms.servicios.previred.exceptions.ClientePreviredException;
import cl.bch.cloud.ms.servicios.previred.exceptions.PreviredException;
import cl.bch.cloud.ms.servicios.previred.utils.CleanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class ExceptionHandlerController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionHandlerController.class);
    
    /**
     * Captura todas las excepciones no genericas
     *
     * @param req HttpServletRequest del controller
     * @param ex  excepcion capturada
     * @return ResponseEntity con objeto que contiene mensaje catalogado dependiendo de la excepcion
     */
    @ExceptionHandler({PreviredException.class})
    ResponseEntity<ErrorInfoDTO> handleConsultaProductosException(HttpServletRequest req, PreviredException ex) {
        CodigosCatalogoEnum catalogo = CodigosCatalogoEnum.getCodigoValeOf(ex.getMessage());
        creaLogError(req.getRequestURL().toString(), catalogo.getCodigo(), ex);
        return new ResponseEntity<>(creaErrorInfoPorCodigo(catalogo), catalogo.getHttpStatus());
    }

    /**
     * Captura todas las excepciones genericas
     *
     * @param req HttpServletRequest del controller
     * @param ex  excepcion capturada
     * @return ResponseEntity con objeto que contiene mensaje catalogado dependiendo de la excepcion
     */
    @ExceptionHandler(Exception.class)
    ResponseEntity<ErrorInfoDTO> handleGlobalExceptionRequest(HttpServletRequest req, Exception ex) {
        CodigosCatalogoEnum catalogo = CodigosCatalogoEnum.getCodigoValeOf(
                CodigosCatalogoEnum.APP_ERROR_GENERAL.getCodigo());
        creaLogError(req.getRequestURL().toString(), catalogo.getCodigo(), ex);
        return new ResponseEntity<>(creaErrorInfoPorCodigo(catalogo), catalogo.getHttpStatus());
    }

    /**
     * Captura todas las excepciones de validaciones en los datos de entrada
     *
     * @param req HttpServletRequest del controller
     * @param ex  excepcion capturada
     * @return ResponseEntity con objeto que contiene mensaje catalogado dependiendo de la excepcion
     */

    @ExceptionHandler(MethodArgumentNotValidException.class)
    ResponseEntity<ErrorInfoDTO> handleValidationDataException(HttpServletRequest req, Exception ex) {
        CodigosCatalogoEnum catalogo = CodigosCatalogoEnum.getCodigoValeOf(
                CodigosCatalogoEnum.APP_DATOS_FALTANTES.getCodigo());
        creaLogError(req.getRequestURL().toString(), catalogo.getCodigo(), ex);
        return new ResponseEntity<>(creaErrorInfoPorCodigo(catalogo), catalogo.getHttpStatus());
    }


    /**
     * Captura las excepciones del cliente de consulta a previred, 
     * es customizada para mensaje personalizado de error
     *
     * @param req HttpServletRequest del controller
     * @param ex  excepcion capturada
     * @return ResponseEntity con objeto que contiene mensaje catalogado dependiendo de la excepcion
     */

    @ExceptionHandler(ClientePreviredException.class)
    ResponseEntity<ErrorInfoDTO> handleClientePreviredException(HttpServletRequest req, Exception ex) {
        ClientePreviredException clientePreviredException = (ClientePreviredException) ex;
        CodigosCatalogoEnum catalogo = CodigosCatalogoEnum.getCodigoValeOf(clientePreviredException.getMessage());
        catalogo.setMensaje(
                catalogo.getMensaje().replace("{CODIGO_ERROR}", 
                        ((ClientePreviredException) ex).getCodigoErrorServicio()));
        creaLogError(req.getRequestURL().toString(), catalogo.getCodigo(), ex);
        return new ResponseEntity<>(creaErrorInfoPorCodigo(catalogo), catalogo.getHttpStatus());
    }

    private static void creaLogError(String requestUrl, String errorInternalCode, Exception ex) {
        LOGGER.error("{} {} {}",
                CleanUtils.clean(requestUrl),
                CleanUtils.clean(errorInternalCode),
                CleanUtils.clean(ex.getMessage()));
    }

    private static ErrorInfoDTO creaErrorInfoPorCodigo(CodigosCatalogoEnum catalogo){
        return new ErrorInfoDTO(
                catalogo.getCodigo(),
                catalogo.getMensaje());
    }

}