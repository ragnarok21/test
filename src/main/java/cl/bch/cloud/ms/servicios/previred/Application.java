package cl.bch.cloud.ms.servicios.previred;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication(scanBasePackages = {"cl.bch.cloud.ms.servicios.previred", "cl.bancochile.ws.client.previred"})
@PropertySources({
        @PropertySource("classpath:osb.properties"),
        @PropertySource("classpath:rest.properties"),
        @PropertySource("classpath:bbdd.properties"),
        @PropertySource("classpath:ms-servicios-previred.properties")
})
@EnableFeignClients
@EnableAsync
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}