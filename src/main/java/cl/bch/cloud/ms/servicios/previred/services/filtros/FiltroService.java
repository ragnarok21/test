package cl.bch.cloud.ms.servicios.previred.services.filtros;


import cl.bch.cloud.ms.servicios.previred.dtos.PreviredDTO;

public interface FiltroService {
    
    void applyFilter(PreviredDTO entradaPrevired);
    
}
