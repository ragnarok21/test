package cl.bch.cloud.ms.servicios.previred.exceptions;

public class ClientePreviredException extends RuntimeException {

    private static final long serialVersionUID = 804701626351693218L;
    
    private final String codigoErrorServicio;

    public ClientePreviredException(String message, String codigoErrorServicio) {
        super(message);
        this.codigoErrorServicio = codigoErrorServicio;
    }

    public String getCodigoErrorServicio() {
        return codigoErrorServicio;
    }
}
