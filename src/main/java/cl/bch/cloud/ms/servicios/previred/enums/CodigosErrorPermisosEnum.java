package cl.bch.cloud.ms.servicios.previred.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.Optional;

@AllArgsConstructor
@Getter
public enum CodigosErrorPermisosEnum {

    CODE_100(100, CodigosCatalogoEnum.PERMISO_USUARIO_NO_EXISTE),
    CODE_101(101, CodigosCatalogoEnum.PERMISO_USUARIO_NO_VALIDO),
    CODE_102(102, CodigosCatalogoEnum.PERMISO_USUARIO_NO_ACTIVO),
    CODE_103(103, CodigosCatalogoEnum.PERMISO_EMPRESA_NO_ACTIVA),
    CODE_104(104, CodigosCatalogoEnum.PERMISO_CANAL_NO_ACTIVO),
    CODE_105(105, CodigosCatalogoEnum.PERMISO_FORZAR_COMPRA_NO_PERMITIDA),
    CODE_106(106, CodigosCatalogoEnum.PERMISO_MAXIMO_CONSULTAS_PERMITIDAS);

    private final int codigo;
    private final CodigosCatalogoEnum codigosCatalogoEnum;

    public static Optional<CodigosErrorPermisosEnum> getCodigoValueOf(int codigo) {
        return Arrays.stream(CodigosErrorPermisosEnum.values())
                .filter(p -> codigo == p.getCodigo())
                .findFirst();
    }


}
