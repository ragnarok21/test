package cl.bch.cloud.ms.servicios.previred.properties;

import lombok.Data;

@Data
public class PreviredProperties {

    private int diaInicioCompra;
    private int diaFinCompra;
    private String prefijo;
    private String nroCliente;
}
