package cl.bch.cloud.ms.servicios.previred.annotation;

import cl.bch.cloud.ms.servicios.previred.enums.FiltroEnum;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface FiltroStrategy {

    FiltroEnum tipoFiltro();

}
