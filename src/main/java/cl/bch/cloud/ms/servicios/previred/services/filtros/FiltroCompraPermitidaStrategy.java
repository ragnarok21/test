package cl.bch.cloud.ms.servicios.previred.services.filtros;


import cl.bch.cloud.ms.servicios.previred.annotation.FiltroStrategy;
import cl.bch.cloud.ms.servicios.previred.dtos.PreviredDTO;
import cl.bch.cloud.ms.servicios.previred.enums.CodigosCatalogoEnum;
import cl.bch.cloud.ms.servicios.previred.enums.CodigosErrorPermisosEnum;
import cl.bch.cloud.ms.servicios.previred.enums.FiltroEnum;
import cl.bch.cloud.ms.servicios.previred.exceptions.ClientePreviredException;
import cl.bch.cloud.ms.servicios.previred.exceptions.PreviredException;
import cl.bch.cloud.ms.servicios.previred.repositories.ValidaUsuarioDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.io.UncheckedIOException;
import java.math.BigDecimal;
import java.util.Map;
import java.util.Optional;

@Service
@FiltroStrategy(tipoFiltro = FiltroEnum.FILTRO_COMPRA_PERMITIDA)
public class FiltroCompraPermitidaStrategy implements FiltroService {

    private static final Logger LOGGER = LoggerFactory.getLogger(FiltroCompraPermitidaStrategy.class);

    private static final String CODIGO_MENSAJE = "OUT_COD_ERROR";


    @Autowired
    private ValidaUsuarioDao validaUsuarioDao;

    @Override
    public void applyFilter(PreviredDTO entradaPrevired) {

        Map<String, Object> filtroCore = validaUsuarioDao.validaUsuario(entradaPrevired);
        BigDecimal codigo = (BigDecimal) filtroCore.get(CODIGO_MENSAJE);

        Optional<CodigosErrorPermisosEnum> codigoErrorPermiso =
                CodigosErrorPermisosEnum.getCodigoValueOf(codigo.intValue());

        LOGGER.info("Codigo entregado por el filtro de permisos es {}",codigo.intValue());

        if (codigo.intValue() != 0) {
            codigoErrorPermiso.ifPresentOrElse((value) -> {
                        throw new PreviredException(codigoErrorPermiso.get().getCodigosCatalogoEnum().getCodigo());
                    },
                    () -> {
                        throw new PreviredException(CodigosCatalogoEnum
                                .APP_ERROR_GENERAL.getCodigo());
                    });

        } else {
            entradaPrevired.getInformacionConsumidor().setTipoServicio((String) filtroCore.get("OUT_SERVICIO"));
            entradaPrevired.getInformacionConsumidor().setEmpresa((String) filtroCore.get("OUT_EMPRESA"));
            entradaPrevired.getInformacionConsumidor().setUsuarioPrevired((String) filtroCore.get("OUT_SERVICIO_USU"));
            entradaPrevired.getInformacionConsumidor()
                    .setPasswordPrevired((String) filtroCore.get("OUT_SERVICIO_PASS"));
        }
    }
}
