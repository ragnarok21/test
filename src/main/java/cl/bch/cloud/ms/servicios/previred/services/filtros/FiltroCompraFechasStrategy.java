package cl.bch.cloud.ms.servicios.previred.services.filtros;

import cl.bch.cloud.ms.servicios.previred.annotation.FiltroStrategy;
import cl.bch.cloud.ms.servicios.previred.dtos.PreviredDTO;
import cl.bch.cloud.ms.servicios.previred.enums.CodigosCatalogoEnum;
import cl.bch.cloud.ms.servicios.previred.enums.FiltroEnum;
import cl.bch.cloud.ms.servicios.previred.exceptions.PreviredException;
import cl.bch.cloud.ms.servicios.previred.properties.PreviredProperties;
import cl.bch.cloud.ms.servicios.previred.repositories.ConsultaRegistroPorFechaDao;
import cl.bch.cloud.ms.servicios.previred.utils.CleanUtils;
import cl.bch.cloud.ms.servicios.previred.utils.FechaUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Date;

/**
 * Evalua si existe alguna compra relaizada entre los dias parametrizados (13 mes a y 14 mes siguiente ),
 * si no existen compras, se continua con el flujo
 */
@Service
@FiltroStrategy(tipoFiltro = FiltroEnum.FILTRO_COMPRA_FECHAS)
public class FiltroCompraFechasStrategy implements FiltroService {

    @Autowired
    private ConsultaRegistroPorFechaDao consultaRegistroPorFechaDao;

    @Autowired
    private PreviredProperties previredProperties;

    @Autowired
    private FechaUtils fechaUtils;

    private static final Logger LOGGER = LoggerFactory.getLogger(FiltroCompraDiariaStrategy.class);

    @Override
    public void applyFilter(PreviredDTO previredDTO) {

        final LocalDate dateNow = fechaUtils.getDateNow();

        if (consultaRegistroPorFechaDao.existeRegistro(
                Integer.parseInt(CleanUtils.cleanRutSinGuionPuntosDV(previredDTO.getRutClienteCompra())),
                obtenerFechaInicio(dateNow), obtenerFechaFin(dateNow))) {
            LOGGER.error("cliente ya realizo compra");
            throw new PreviredException(CodigosCatalogoEnum.APP_COMPRA_EXISTENTE.getCodigo(),
                    new Exception("Cliente ya realizó una compra"));
        }
    }


    private Date obtenerFechaInicio(LocalDate dateNow) {

        /* Evalua si tomar el rango de fechas del mes anterior o el mes en curso  */
        if (dateNow.getDayOfMonth() < previredProperties.getDiaInicioCompra()) {

            /* Se evalua fecha por si cambia de año al quitar un mes  */
            LocalDate fechaMesAnterior = dateNow.minusMonths(1);
            return fechaUtils.localDateToDate(
                    LocalDate.of(
                            fechaMesAnterior.getYear(),
                            fechaMesAnterior.getMonthValue(),
                            previredProperties.getDiaInicioCompra()));
        } else {
            return fechaUtils.localDateToDate(
                    LocalDate.of(
                            dateNow.getYear(),
                            dateNow.getMonthValue(),
                            previredProperties.getDiaInicioCompra()));
        }

    }

    private Date obtenerFechaFin(LocalDate dateNow) {

        /* Evalua si tomar el rango de fechas del mes posterior o el mes en curso  */
        if (dateNow.getDayOfMonth() < previredProperties.getDiaInicioCompra()) {
            return fechaUtils.localDateToDate(
                    LocalDate.of(
                            dateNow.getYear(),
                            dateNow.getMonthValue(),
                            previredProperties.getDiaFinCompra()));

        } else {
            /* Se evalua fecha por si cambia de año al sumar un mes */
            LocalDate fechaMesPosterior = dateNow.plusMonths(1);
            return fechaUtils.localDateToDate(
                    LocalDate.of(
                            fechaMesPosterior.getYear(),
                            fechaMesPosterior.getMonthValue(),
                            previredProperties.getDiaFinCompra()));
        }
    }


}
