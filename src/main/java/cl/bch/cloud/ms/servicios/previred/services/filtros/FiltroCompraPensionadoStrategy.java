package cl.bch.cloud.ms.servicios.previred.services.filtros;

import cl.bch.cloud.ms.servicios.previred.annotation.FiltroStrategy;
import cl.bch.cloud.ms.servicios.previred.dtos.PreviredDTO;
import cl.bch.cloud.ms.servicios.previred.enums.CodigosCatalogoEnum;
import cl.bch.cloud.ms.servicios.previred.enums.FiltroEnum;
import cl.bch.cloud.ms.servicios.previred.exceptions.PreviredException;
import cl.bch.cloud.ms.servicios.previred.repositories.ConsultaRegistroUltimosMesesDao;
import cl.bch.cloud.ms.servicios.previred.restClients.PensionadoApiClient;
import cl.bch.cloud.ms.servicios.previred.utils.CleanUtils;
import cl.bch.cloud.ms.servicios.previred.utils.FechaUtils;
import feign.FeignException.FeignClientException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
@FiltroStrategy(tipoFiltro = FiltroEnum.FILTRO_COMPRA_PENSIONADO)
public class FiltroCompraPensionadoStrategy implements FiltroService {

    private static final Logger LOGGER = LoggerFactory.getLogger(FiltroCompraPensionadoStrategy.class);

    @Autowired
    private ConsultaRegistroUltimosMesesDao consultaRegistroUltimosMesesDao;

    @Autowired
    private FechaUtils fechaUtils;

    @Autowired
    private PensionadoApiClient pensionadoApiClient;


    public void applyFilter(PreviredDTO entradaPrevired) {

        String rutCliente = entradaPrevired.getRutClienteCompra();

        try {

            if (!pensionadoApiClient.esPensionado(CleanUtils.cleanRutSinGuionPuntos(rutCliente))) {
                Map<String, Object> registro = consultaRegistroUltimosMesesDao
                        .obtenerUltimoRegistro(CleanUtils.cleanRutSinGuionPuntosDV(rutCliente));
                List detalleRegistro = (List) registro.get("OUT_DETALLES");

                if (!detalleRegistro.isEmpty()) {
                    Map<String, Object> resultMap = (Map<String, Object>) detalleRegistro.get(0);

                    if (compararCodigosSinCotizaciones(resultMap.get("CODIGO").toString())
                            && intervaloFechaCompraInvalida((Date) resultMap.get("TIMESTAMP"))) {
                        throw new PreviredException(CodigosCatalogoEnum.APP_CLIENTE_PENSIONADO.getCodigo());
                    }
                }
            } else {
                LOGGER.error("Cliente es pensionado");
                throw new PreviredException(CodigosCatalogoEnum.APP_CLIENTE_PENSIONADO.getCodigo());
            }

        } catch (FeignClientException e) {
            LOGGER.error("Error al invocar Feign Client para MS Pensionados");
            throw new PreviredException(CodigosCatalogoEnum.APP_ERROR_GENERAL.getCodigo());
        }

    }

    private boolean compararCodigosSinCotizaciones(String codigo) {
        return "9360".equals(codigo) || "9365".equals(codigo);
    }

    private boolean intervaloFechaCompraInvalida(Date fechaBaseDatos) {
        LocalDate localDate = fechaUtils.dateToLocalDate(fechaBaseDatos);
        return (localDate.isBefore(fechaUtils.getDateNow())
                || localDate.isEqual(fechaUtils.getDateNow()))
                && localDate.isAfter(fechaUtils.getDateNow().minusMonths(4));

    }
}