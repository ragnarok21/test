package cl.bch.cloud.ms.servicios.previred.sal;

import cl.bch.cloud.ms.servicios.previred.dtos.ClienteCompraPreviredResponse;
import cl.bch.cloud.ms.servicios.previred.dtos.PreviredDTO;

import java.sql.Timestamp;

public interface ClienteWsPrevired {
    ClienteCompraPreviredResponse callWs(PreviredDTO entradaPrevired, Timestamp timestamp);
}
