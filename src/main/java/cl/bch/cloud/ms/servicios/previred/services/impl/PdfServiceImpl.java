package cl.bch.cloud.ms.servicios.previred.services.impl;

import cl.bch.cloud.ms.servicios.previred.services.PdfService;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;

@Service
public class PdfServiceImpl implements PdfService {

    @Override
    @Async
    public void firmarDocumentoToc(Timestamp timestamp) {
        System.out.println(Thread.currentThread().getId());
        System.out.println("firmando documento");
    }
}
