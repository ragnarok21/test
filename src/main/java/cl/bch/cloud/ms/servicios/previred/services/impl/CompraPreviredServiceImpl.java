package cl.bch.cloud.ms.servicios.previred.services.impl;

import cl.bch.cloud.ms.servicios.previred.dtos.ClienteCompraPreviredResponse;
import cl.bch.cloud.ms.servicios.previred.dtos.PreviredDTO;
import cl.bch.cloud.ms.servicios.previred.enums.FiltroEnum;
import cl.bch.cloud.ms.servicios.previred.repositories.GuardarXmlPreviredDao;
import cl.bch.cloud.ms.servicios.previred.sal.ClienteWsPrevired;
import cl.bch.cloud.ms.servicios.previred.services.CompraPreviredService;
import cl.bch.cloud.ms.servicios.previred.services.PdfService;
import cl.bch.cloud.ms.servicios.previred.services.filtros.FiltroFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;


@Service
public class CompraPreviredServiceImpl implements CompraPreviredService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CompraPreviredServiceImpl.class);

    @Autowired
    private FiltroFactory filtroFactory;

    @Autowired
    private GuardarXmlPreviredDao guardarXmlPreviredDao;

    @Autowired
    private ClienteWsPrevired clienteWsPrevired;

    @Autowired
    private PdfService pdfService;


    @Override
    public void comprarRenta(Boolean forzarCompraSinFiltros, PreviredDTO entradaPrevired) {

        entradaPrevired.setForzar(forzarCompraSinFiltros);

        if (forzarCompraSinFiltros) {
            filtroFactory.obtenerFiltro(FiltroEnum.FILTRO_COMPRA_PERMITIDA, entradaPrevired);
            filtroFactory.obtenerFiltro(FiltroEnum.FILTRO_COMPRA_DIARIA, entradaPrevired);
            filtroFactory.obtenerFiltro(FiltroEnum.FILTRO_LLAVE_COMPRA, entradaPrevired);
        }else {
            filtroFactory.obtenerFiltro(FiltroEnum.FILTRO_COMPRA_PERMITIDA, entradaPrevired);
            filtroFactory.obtenerFiltro(FiltroEnum.FILTRO_COMPRA_DIARIA, entradaPrevired);
            filtroFactory.obtenerFiltro(FiltroEnum.FILTRO_COMPRA_FECHAS, entradaPrevired);
            filtroFactory.obtenerFiltro(FiltroEnum.FILTRO_COMPRA_PENSIONADO, entradaPrevired);
            filtroFactory.obtenerFiltro(FiltroEnum.FILTRO_LLAVE_COMPRA, entradaPrevired);
        }

        long currentTimeCompra = System.currentTimeMillis();
        Timestamp timestamp = new Timestamp(currentTimeCompra);

        pdfService.firmarDocumentoToc(timestamp);
        ClienteCompraPreviredResponse clienteCompraPreviredResponse =
                clienteWsPrevired.callWs(entradaPrevired, timestamp);
        LOGGER.info("xml salida : {}", clienteCompraPreviredResponse.getXml());
        guardarXmlPreviredDao.saveXML(entradaPrevired, timestamp, clienteCompraPreviredResponse);
    }
}
