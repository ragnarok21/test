package cl.bch.cloud.ms.servicios.previred.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.Optional;

@AllArgsConstructor
@Getter
public enum CodigosErrorWsPreviredEnum {

    CODE_9030("9030", CodigosCatalogoEnum.PREVIRED_WS_LLAVE_NO_EXISTE),
    CODE_9040("9040", CodigosCatalogoEnum.PREVIRED_WS_LLAVE_NO_VALIDA),
    CODE_9330("9330", CodigosCatalogoEnum.PREVIRED_WS_MANDATO_UTILIZADO),
    CODE_9350("9350", CodigosCatalogoEnum.PREVIRED_WS_SIN_INFORMACION_MOMENTANEA),
    CODE_9355("9355", CodigosCatalogoEnum.PREVIRED_WS_PETICION_FUERA_HORARIO),
    CODE_9374("9374", CodigosCatalogoEnum.PREVIRED_WS_CODIGO_AUT_INVALIDO),
    CODE_9376("9376", CodigosCatalogoEnum.PREVIRED_WS_CODIGO_AUT_INCORRECTO);

    private final String codigo;
    private final CodigosCatalogoEnum codigosCatalogoEnum;

    public static Optional<CodigosErrorWsPreviredEnum> getCodigoValueOf(String codigo) {
        return Arrays.stream(CodigosErrorWsPreviredEnum.values())
                .filter(p -> codigo.equals(p.getCodigo()))
                .findFirst();
    }


}
