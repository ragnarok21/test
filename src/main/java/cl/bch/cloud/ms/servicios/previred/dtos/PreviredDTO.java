package cl.bch.cloud.ms.servicios.previred.dtos;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.math.BigDecimal;

@Data
public class PreviredDTO {

    @NotEmpty
    private String usernameSolicitante;
    @NotEmpty
    @Pattern(regexp = "^([0-9]){1,2}.([0-9]){3}.([0-9]){3}-[\\d|(k|K)]", message = "Rut cliente no válido")
    private String rutClienteCompra;
    @NotNull
    @Valid
    private ConsumidorDTO informacionConsumidor;
    private BigDecimal idCompra;
    private Boolean forzar;

}
