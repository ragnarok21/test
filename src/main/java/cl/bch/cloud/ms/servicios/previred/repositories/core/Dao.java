package cl.bch.cloud.ms.servicios.previred.repositories.core;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import javax.sql.DataSource;

public abstract class Dao {

    @Autowired
    private DataSource dataSource;

    private SimpleJdbcCall simpleJdbcCall;

    public void setSimpleJdbcCall(SimpleJdbcCall simpleJdbcCall) {
        this.simpleJdbcCall = simpleJdbcCall;
    }

    public SimpleJdbcCall getSimpleJdbcCall() {
        return simpleJdbcCall;
    }

    protected abstract String name();

    protected abstract SqlParameter[] parameters();

    protected String catalogName() {
        return "PREVIRED";
    }

    public final DataSource getDataSource() {
        return this.dataSource;
    }
}
