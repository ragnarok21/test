package cl.bch.cloud.ms.servicios.previred.config;

import cl.bch.cloud.ms.servicios.previred.properties.PreviredProperties;
import cl.bch.cloud.ms.servicios.previred.properties.SwaggerProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PropertiesConfig {

    @Bean
    @ConfigurationProperties(prefix = "previred")
    public PreviredProperties previredProperties() {
        return new PreviredProperties();
    }

    @Bean
    @ConfigurationProperties(prefix = "swagger")
    public SwaggerProperties swaggerProperties() {
        return new SwaggerProperties();
    }

}
