package cl.bch.cloud.ms.servicios.previred.repositories;

import cl.bch.cloud.ms.servicios.previred.dtos.ClienteCompraPreviredResponse;
import cl.bch.cloud.ms.servicios.previred.dtos.PreviredDTO;

import java.sql.Timestamp;

public interface GuardarXmlPreviredDao {

    void saveXML(PreviredDTO previredDTO, Timestamp timestamp, 
                 ClienteCompraPreviredResponse clienteCompraPreviredResponse);
}
