package cl.bch.cloud.ms.servicios.previred.sal.impl;


import cl.bancochile.ws.client.previred.ClienteWsCompraPreviredService;
import cl.bancochile.ws.client.previred.domain.request.ClientePreviredRequest;
import cl.bancochile.ws.client.previred.domain.response.Respuesta;
import cl.bancochile.ws.client.previred.domain.response.RespuestaServicio;
import cl.bch.cloud.ms.servicios.previred.dtos.ClienteCompraPreviredResponse;
import cl.bch.cloud.ms.servicios.previred.dtos.PreviredDTO;
import cl.bch.cloud.ms.servicios.previred.enums.CodigosCatalogoEnum;
import cl.bch.cloud.ms.servicios.previred.enums.CodigosErrorWsPreviredEnum;
import cl.bch.cloud.ms.servicios.previred.exceptions.ClientePreviredException;
import cl.bch.cloud.ms.servicios.previred.exceptions.PreviredException;
import cl.bch.cloud.ms.servicios.previred.sal.ClienteWsPrevired;
import cl.bch.cloud.ms.servicios.previred.utils.CleanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBException;
import java.rmi.RemoteException;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;


@Service
public class ClienteWsPreviredImpl implements ClienteWsPrevired {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClienteWsPreviredImpl.class);

    private static final String SUCCESSFULL_GENERAL_CODE = "9000";
    

    private static final HashSet<String> exceptionalErrorCode;

    static {
        //Codigos de error excepcionales para clientes sin cotizaciones
        exceptionalErrorCode = new HashSet<>();
        exceptionalErrorCode.addAll(Arrays.asList("9360", "9365"));
    }


    @Autowired
    private ClienteWsCompraPreviredService wsCompraPreviredService;

    public ClienteCompraPreviredResponse callWs(PreviredDTO previredDTO, Timestamp timestamp) {

        LOGGER.info("Llamando Cliente WS Compra Previred");
        String filteredCode = SUCCESSFULL_GENERAL_CODE;
        try {
            Respuesta respuesta = wsCompraPreviredService.obtenerCotizacionesPrevired(
                    new ClientePreviredRequest.ClientePreviredRequestBuilder()
                            .withRutCliente(CleanUtils.cleanRutSinPuntos(previredDTO.getRutClienteCompra()))
                            .withLlave(previredDTO.getInformacionConsumidor().getLlave())
                            .withTipoServicio(previredDTO.getInformacionConsumidor().getTipoServicio())
                            .withUsuario(previredDTO.getInformacionConsumidor().getUsuarioPrevired())
                            .withPassword(previredDTO.getInformacionConsumidor().getPasswordPrevired())
                            .withTimestamp(timestamp.toString())
                            .build());

            if(!SUCCESSFULL_GENERAL_CODE.equals(respuesta.getControl().getCodigo())) {
                Optional<RespuestaServicio> respuestaServicioFounded = respuesta.getRespuestaServicioList()
                        .stream()
                        //TODO OBTENER TIPO DE REGISTRO DESDE ConsumidorDTO CON EL TIPO
                        .filter(respuestaServicio -> previredDTO.getInformacionConsumidor().getTipoServicio()
                                        .equals(respuestaServicio.getTipo()))
                        .findFirst();

                filteredCode = respuestaServicioFounded.get().getControl().getCodigo();
                LOGGER.info("Codigo de error del servicio de previred {}, para filtrar",filteredCode);

                Optional<CodigosErrorWsPreviredEnum> codigoValueOf = CodigosErrorWsPreviredEnum
                        .getCodigoValueOf(filteredCode);

                final String finalFilteredCode = filteredCode;
                codigoValueOf.ifPresentOrElse((value) -> {
                            throw new PreviredException(value.getCodigosCatalogoEnum().getCodigo());
                        },
                        () -> {
                            if (!exceptionalErrorCode.contains(finalFilteredCode)) {
                                throw new ClientePreviredException(CodigosCatalogoEnum
                                        .PREVIRED_WS_ERROR_DINAMICO.getCodigo(), finalFilteredCode);
                            }
                        });
            }

            return new ClienteCompraPreviredResponse(filteredCode, respuesta.getXmlRespuesta());
            
        } catch (RemoteException | JAXBException e) {
            LOGGER.error("Error al invocar al servicio {}", e.getMessage());
            throw new PreviredException(
                    CodigosCatalogoEnum.APP_ERROR_GENERAL.getCodigo(), new Exception("Error servicio previred"));
        }
    }
}
