package cl.bch.cloud.ms.servicios.previred.utils;

import io.micrometer.core.instrument.util.StringUtils;
import org.owasp.encoder.Encode;

public class CleanUtils {

    private CleanUtils() {
        //constructor privado
    }

    public static String clean(String input) {
        return Encode.forHtml(removeNewLines(input));
    }

    private static String removeNewLines(String input) {
        return StringUtils.isNotEmpty(input) 
                ? input.replace("\n", " ").replace("\r", " ") 
                : input;
    }

    public static String cleanRutSinPuntos(String rut) {
        return rut.replaceAll("[\\.]", "");
    }
    
    public static String cleanRutSinDV(String rut){
        return cleanRutSinPuntos(rut).split("-")[0];
    }

    public static String cleanRutSinGuionPuntos(String rut) { return rut.replaceAll("[-+\\.]","");}
    
    public static String cleanRutSinGuionPuntosDV(String rut) {
        return cleanRutSinDV(rut).replaceAll("[-+\\.]","");}
    
}
