package cl.bch.cloud.ms.servicios.previred.repositories;

import java.util.Map;

public interface ConsultaRegistroUltimosMesesDao {

	Map<String, Object> obtenerUltimoRegistro(String rut);

}
