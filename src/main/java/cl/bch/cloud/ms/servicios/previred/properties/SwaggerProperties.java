package cl.bch.cloud.ms.servicios.previred.properties;

import lombok.Data;
import lombok.experimental.Delegate;

@Data
public class SwaggerProperties {

    @Delegate
    private SwaggerUiEnabled ui;

    @Delegate
    private SwaggerInfo info;


    @Data
    private static class SwaggerUiEnabled {
        private boolean enabled;
    }

    @Data
    private static class SwaggerInfo {

        private String name;
        private String description;
        private String version;
        private SwaggerInfoContact contact;
    }

}
