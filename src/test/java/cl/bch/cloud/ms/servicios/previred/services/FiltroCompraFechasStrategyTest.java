package cl.bch.cloud.ms.servicios.previred.services;

import cl.bch.cloud.ms.servicios.previred.dtos.PreviredDTO;
import cl.bch.cloud.ms.servicios.previred.exceptions.PreviredException;
import cl.bch.cloud.ms.servicios.previred.properties.PreviredProperties;
import cl.bch.cloud.ms.servicios.previred.repositories.ConsultaRegistroPorFechaDao;
import cl.bch.cloud.ms.servicios.previred.services.filtros.FiltroCompraFechasStrategy;
import cl.bch.cloud.ms.servicios.previred.utils.FechaUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
public class FiltroCompraFechasStrategyTest {
    
    @Mock
    private ConsultaRegistroPorFechaDao consultaRegistroPorFechaDao;

    @Mock
    private PreviredProperties previredProperties;

    @Mock
    private FechaUtils fechaUtils;
    
    @InjectMocks
    private FiltroCompraFechasStrategy fechasStrategy;

    @BeforeEach
    void init() {
        when(previredProperties.getDiaInicioCompra()).thenReturn(14);
        when(previredProperties.getDiaFinCompra()).thenReturn(13);
        when(fechaUtils.localDateToDate(any(LocalDate.class))).thenCallRealMethod();

    }


    @Test
    void APPLY_FILTER_FILTRO_COMPRA_FECHAS_INFERIOR_TEST() {

        PreviredDTO previredDTO = new PreviredDTO();
        previredDTO.setRutClienteCompra("1-9");

        LocalDate now = LocalDate.of(2020, 7, 13);
        when(fechaUtils.getDateNow()).thenReturn(now);

        // similar a hacer un assert equals
        Date eqFechaInicio = localDateToDate(LocalDate.of(2020, 6, 14));
        Date eqFin = localDateToDate(LocalDate.of(2020, 7, 13));

        when(consultaRegistroPorFechaDao.existeRegistro(
                any(Integer.class),
                eq(eqFechaInicio),
                eq(eqFin)))
                .thenReturn(Boolean.FALSE);

        fechasStrategy.applyFilter(previredDTO);

    }


    @Test
    void APPLY_FILTER_FILTRO_COMPRA_FECHAS_SUPERIOR_TEST() {

        PreviredDTO previredDTO = new PreviredDTO();
        previredDTO.setRutClienteCompra("1-9");

        LocalDate now = LocalDate.of(2020, 7, 15);
        when(fechaUtils.getDateNow()).thenReturn(now);

        // similar a hacer un assert equals
        Date eqFechaInicio = localDateToDate(LocalDate.of(2020, 7, 14));
        Date eqFin = localDateToDate(LocalDate.of(2020, 8, 13));

        when(consultaRegistroPorFechaDao.existeRegistro(
                any(Integer.class),
                eq(eqFechaInicio),
                eq(eqFin)))
                .thenReturn(Boolean.FALSE);

        fechasStrategy.applyFilter(previredDTO);

    }


    @Test
    void APPLY_FILTER_FILTRO_COMPRA_FECHAS_EXISTENTE_TEST() {

        PreviredDTO previredDTO = new PreviredDTO();
        previredDTO.setRutClienteCompra("1-9");

        LocalDate now = LocalDate.of(2020, 7, 15);
        when(fechaUtils.getDateNow()).thenReturn(now);


        when(consultaRegistroPorFechaDao.existeRegistro(
                any(Integer.class),
                any(Date.class),
                any(Date.class))).thenReturn(Boolean.TRUE);


        Assertions.assertThrows(PreviredException.class, () -> {
            fechasStrategy.applyFilter(previredDTO);
        });

    }


    public Date localDateToDate(LocalDate localDate) {
        return Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }
    
}
