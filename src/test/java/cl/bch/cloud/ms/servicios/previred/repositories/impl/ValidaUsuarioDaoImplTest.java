package cl.bch.cloud.ms.servicios.previred.repositories.impl;

import cl.bch.cloud.ms.servicios.previred.dtos.ConsumidorDTO;
import cl.bch.cloud.ms.servicios.previred.dtos.PreviredDTO;
import cl.bch.cloud.ms.servicios.previred.exceptions.PreviredException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ValidaUsuarioDaoImplTest {

    @InjectMocks
    private ValidaUsuarioDaoImpl repository;

    @Mock
    private SimpleJdbcCall simpleJdbcCall;

    @BeforeEach
    void before() {
        repository.setSimpleJdbcCall(this.simpleJdbcCall);
    }

    @Test
    public void VALIDA_USUARIO_TEST() {

        Map<String, Object> hashMap = new HashMap<>();
        hashMap.put("OUT_COD_ERROR", new BigDecimal(100));
        hashMap.put("OUT_MENSAJE", "NOK: USUARIO NO EXISTE");

        MapSqlParameterSource inParams = new MapSqlParameterSource()
                .addValue("IN_CANAL", "sucursal")
                .addValue("IN_USUARIO", "juanito")
                .addValue("IN_REGISTRO", "12lklfdkqwerta123asd==")
                .addValue("IN_FORZAR", 1);
        when(repository.execute(any(MapSqlParameterSource.class))).thenReturn(hashMap);
        PreviredDTO previredDTO = new PreviredDTO();
        previredDTO.setForzar(Boolean.FALSE);
        ConsumidorDTO consumidorDTO = new ConsumidorDTO();
        consumidorDTO.setLlave("1234");
        consumidorDTO.setNombre("test");
        consumidorDTO.setEmpresa("test");
        consumidorDTO.setCanal("test");
        previredDTO.setInformacionConsumidor(consumidorDTO);
        repository.validaUsuario(previredDTO);
    }

    @Test
    void VALIDAR_USUARIO_ERROR_TEST() {

        DataAccessException dataAccessException = Mockito.mock(DataAccessException.class);

        when(repository.execute(any(MapSqlParameterSource.class))).thenThrow(dataAccessException);

        PreviredDTO previredDTO = new PreviredDTO();
        previredDTO.setForzar(Boolean.TRUE);
        ConsumidorDTO consumidorDTO = new ConsumidorDTO();
        consumidorDTO.setLlave("1234");
        consumidorDTO.setNombre("test");
        consumidorDTO.setEmpresa("test");
        consumidorDTO.setCanal("test");
        previredDTO.setInformacionConsumidor(consumidorDTO);
        
        Assertions.assertThrows(PreviredException.class, () -> {
            repository.validaUsuario(previredDTO);
        });
    }

}
