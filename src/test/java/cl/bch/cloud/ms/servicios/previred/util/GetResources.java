package cl.bch.cloud.ms.servicios.previred.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

public class GetResources {

    private GetResources() {
    }


    public static <T> List<T> GET_MOCK_RESOURCE_LIST(String nameResource, Class<T> listType) throws IOException {
        String mockJsonResponse = GetResources.getMockResourceResponse(nameResource);
        return JsonUtils.convertJsonToObjectList(mockJsonResponse, listType);
    }

    public static <T> T GET_MOCK_RESOURCE(String nameResource, Class<T> type) throws IOException {
        String mockJsonResponse = GetResources.getMockResourceResponse(nameResource);
        return JsonUtils.convertJsonToObject(mockJsonResponse, type);
    }
    public static String getMockResourceResponse(String name){
        StringBuilder sbResp = new StringBuilder();
        try {
            InputStream resourceAsStream = GetResources.class.getResourceAsStream("/"+name);
            InputStreamReader inputStreamReader = new InputStreamReader(resourceAsStream);
            BufferedReader br = new BufferedReader(inputStreamReader);
            String sCurrentLine;
            while ((sCurrentLine = br.readLine()) != null) {
                sbResp.append(sCurrentLine);
            }
            resourceAsStream.close();
            inputStreamReader.close();
            br.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return sbResp.toString();
    }
}
