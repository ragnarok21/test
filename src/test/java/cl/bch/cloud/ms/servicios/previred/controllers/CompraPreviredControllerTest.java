package cl.bch.cloud.ms.servicios.previred.controllers;

import cl.bancochile.ws.client.previred.ClienteWsCompraPreviredService;
import cl.bancochile.ws.client.previred.domain.response.Respuesta;
import cl.bch.cloud.ms.servicios.previred.config.TestContextConfig;
import cl.bch.cloud.ms.servicios.previred.enums.CodigosCatalogoEnum;
import cl.bch.cloud.ms.servicios.previred.exceptions.PreviredException;
import cl.bch.cloud.ms.servicios.previred.repositories.ConsultaRegistroUltimosMesesDao;
import cl.bch.cloud.ms.servicios.previred.repositories.GetFolioDao;
import cl.bch.cloud.ms.servicios.previred.repositories.impl.ConsultaRegistroPorFechaDaoImpl;
import cl.bch.cloud.ms.servicios.previred.repositories.impl.GuardarXmlPreviredDaoImpl;
import cl.bch.cloud.ms.servicios.previred.repositories.impl.ValidaUsuarioDaoImpl;
import cl.bch.cloud.ms.servicios.previred.restClients.PensionadoApiClient;
import cl.bch.cloud.ms.servicios.previred.util.GetResources;
import cl.bch.cloud.ms.servicios.previred.util.XmlUtil;
import feign.FeignException.FeignClientException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.remoting.RemoteAccessException;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = TestContextConfig.class)
@AutoConfigureMockMvc
public class CompraPreviredControllerTest {

    @Autowired
    protected MockMvc mockMvc;

    @MockBean
    private ClienteWsCompraPreviredService wsCompraPreviredService;

    @MockBean
    private ConsultaRegistroPorFechaDaoImpl consultaRegistroPorFechaDao;

    @MockBean
    private GuardarXmlPreviredDaoImpl guardarXmlPreviredDao;

    @MockBean
    private ValidaUsuarioDaoImpl validaUsuarioDao;

    @MockBean
    private GetFolioDao getFolioDao;

    @MockBean
    private ConsultaRegistroUltimosMesesDao consultaRegistroUltimosMesesDao;

    @MockBean
    private PensionadoApiClient pensionadoApiClient;

    private final String request = "{ \"usernameSolicitante\": \"gsuazoc\", \"rutClienteCompra\": \"10.001.596-K\", \"informacionConsumidor\": { \"nombre\": \"bch\", \"canal\": \"web\", \"registro\": \"12lklfdkqwerta123asd==\", \"llave\": \"BCH-11111111-77366829\" } }";
    private final String requestInvalido = "{ \"rutClienteCompra\": \"10.001.596-P\", \"informacionConsumidor\": { \"nombre\": \"bch\", \"canal\": \"web\", \"registro\": \"12lklfdkqwerta123asd==\", \"llave\": \"BCH-11111111-77366829\" } }";

    @BeforeEach
    void init() {

        when(consultaRegistroPorFechaDao.
                existeRegistro(ArgumentMatchers.any(Integer.class),
                        ArgumentMatchers.any(Date.class), ArgumentMatchers.any(Date.class))).thenReturn(Boolean.FALSE);

        when(getFolioDao.getFolio()).thenReturn(BigDecimal.ONE);

        Map<String, Object> map = new LinkedHashMap<>();
        map.put("OUT_COD_ERROR", new BigDecimal(0));
        map.put("OUT_SERVICIO_USU", "1-9");
        map.put("OUT_SERVICIO", "CCX");
        map.put("OUT_EMPRESA", "BCH");
        map.put("OUT_SERVICIO_PASS", "1234");
        when(validaUsuarioDao.validaUsuario(any()))
                .thenReturn(map);
        
        
        Map<String, Object> registro = new HashMap<>();
        List<Map<String, Object>> listaDetalles = new ArrayList<>();
        Map<String, Object> detalle = new HashMap<>();
        Date fechaBD = localDateToDate(LocalDate.of(2020, 7, 26));
        detalle.put("TIMESTAMP", fechaBD);
        detalle.put("CODIGO", "9000");
        listaDetalles.add(detalle);
        registro.put("OUT_DETALLES", listaDetalles);

        when(consultaRegistroUltimosMesesDao.obtenerUltimoRegistro(anyString())).thenReturn(registro);
        when(pensionadoApiClient.esPensionado(anyString())).thenReturn(false);
    }

    @Test
    void OBTENER_COTIZACIONES_TEST() throws Exception {


        String mockResourceResponse = GetResources.getMockResourceResponse("response/cotizaciones.xml");
        Respuesta respuesta = XmlUtil.unmarshallXmlString(mockResourceResponse, Respuesta.class);
        respuesta.setXmlRespuesta(mockResourceResponse);
        when(wsCompraPreviredService.obtenerCotizacionesPrevired(any())).thenReturn(respuesta);

        mockMvc.perform(
                post("/v1/servicios/previred/compra-cotizaciones").param("forzar", "false")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(request))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void OBTENER_COTIZACIONES_FORZAR_COMPRA_TEST() throws Exception {


        String mockResourceResponse = GetResources.getMockResourceResponse("response/cotizaciones.xml");
        Respuesta respuesta = XmlUtil.unmarshallXmlString(mockResourceResponse, Respuesta.class);
        respuesta.setXmlRespuesta(mockResourceResponse);
        when(wsCompraPreviredService.obtenerCotizacionesPrevired(any())).thenReturn(respuesta);

        mockMvc.perform(
                post("/v1/servicios/previred/compra-cotizaciones").param("forzar", "true")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(request))
                .andDo(print())
                .andExpect(status().isOk());
    }


    @Test
    void OBTENER_COTIZACIONES_ERROR_TEST() throws Exception {

        PreviredException previredException = new PreviredException(
                CodigosCatalogoEnum.APP_COMPRA_EXISTENTE.getCodigo(),
                new Exception("cliente ya realizo compra"));

        when(consultaRegistroPorFechaDao.
                existeRegistro(ArgumentMatchers.any(Integer.class),
                        ArgumentMatchers.any(Date.class), ArgumentMatchers.any(Date.class))).thenThrow(previredException);


        mockMvc.perform(
                post("/v1/servicios/previred/compra-cotizaciones").param("forzar", "false")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(request))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.code", is("SPCOMP-0004")));
    }

    @Test
    void OBTENER_COTIZACIONES_ERROR_GENERAL_TEST() throws Exception {

        when(consultaRegistroPorFechaDao.
                existeRegistro(ArgumentMatchers.any(Integer.class),
                        ArgumentMatchers.any(Date.class), ArgumentMatchers.any(Date.class)))
                .thenThrow(new RemoteAccessException("error conexión"));


        mockMvc.perform(
                post("/v1/servicios/previred/compra-cotizaciones").param("forzar", "false")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(request))
                .andDo(print())
                .andExpect(status().is5xxServerError())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.code", is("SPCOMP-0001")));
    }

    @Test
    void OBTENER_COTIZACIONES_ERROR_VALIDACIONES_TEST() throws Exception {

        mockMvc.perform(
                post("/v1/servicios/previred/compra-cotizaciones").param("forzar", "false")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestInvalido))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.code", is("SPCOMP-0002")));
    }

    
    @Test
    void OBTENER_COTIZACIONES_ERRORES_CATALOGADOS_TEST() throws Exception {


        String mockResourceResponse = GetResources.getMockResourceResponse("response/cotizacionesErrorHandler.xml");
        Respuesta respuesta = XmlUtil.unmarshallXmlString(mockResourceResponse, Respuesta.class);
        respuesta.setXmlRespuesta(mockResourceResponse);
        when(wsCompraPreviredService.obtenerCotizacionesPrevired(any())).thenReturn(respuesta);

        mockMvc.perform(
                post("/v1/servicios/previred/compra-cotizaciones").param("forzar", "false")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(request))
                .andDo(print())
                .andExpect(status().is5xxServerError())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.code", is("SPCLIE-0002")));
    }


    @Test
    void OBTENER_COTIZACIONES_ERRORES_NO_CATALOGADOS_TEST() throws Exception {


        String mockResourceResponse = GetResources.getMockResourceResponse("response/cotizacionesErrorNoHandler.xml");
        Respuesta respuesta = XmlUtil.unmarshallXmlString(mockResourceResponse, Respuesta.class);
        respuesta.setXmlRespuesta(mockResourceResponse);
        when(wsCompraPreviredService.obtenerCotizacionesPrevired(any())).thenReturn(respuesta);

        mockMvc.perform(
                post("/v1/servicios/previred/compra-cotizaciones").param("forzar", "false")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(request))
                .andDo(print())
                .andExpect(status().is5xxServerError())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.code", is("SPCLIE-0001")))
                .andExpect(jsonPath("$.message", is(
                        "Error servicio de previred, código de error 9348")));
    }


    @Test
    void OBTENER_COTIZACIONES_ES_JUBILADO_TEST() throws Exception {

        String mockResourceResponse = GetResources.getMockResourceResponse("response/cotizaciones.xml");
        Respuesta respuesta = XmlUtil.unmarshallXmlString(mockResourceResponse, Respuesta.class);
        respuesta.setXmlRespuesta(mockResourceResponse);
        when(wsCompraPreviredService.obtenerCotizacionesPrevired(any())).thenReturn(respuesta);
        when(pensionadoApiClient.esPensionado(anyString())).thenReturn(true);

        mockMvc.perform(
                post("/v1/servicios/previred/compra-cotizaciones").param("forzar", "false")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(request))
                .andDo(print())
                .andExpect(status().isAccepted())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.code", is("SPCOMP-0005")));
    }

    @Test
    void ERROR_PENSIONADO_CLIENT_TEST() throws Exception {

        String mockResourceResponse = GetResources.getMockResourceResponse("response/cotizaciones.xml");
        Respuesta respuesta = XmlUtil.unmarshallXmlString(mockResourceResponse, Respuesta.class);
        respuesta.setXmlRespuesta(mockResourceResponse);
        when(wsCompraPreviredService.obtenerCotizacionesPrevired(any())).thenReturn(respuesta);
        doThrow(FeignClientException.class).when(pensionadoApiClient).esPensionado(anyString());

        mockMvc.perform(
                post("/v1/servicios/previred/compra-cotizaciones").param("forzar", "false")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(request))
                .andDo(print())
                .andExpect(status().is5xxServerError())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.code", is("SPCOMP-0001")));
    }


    private Date localDateToDate(LocalDate localDate) {
        return Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

}