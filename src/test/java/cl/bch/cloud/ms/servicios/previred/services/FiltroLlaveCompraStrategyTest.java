package cl.bch.cloud.ms.servicios.previred.services;

import cl.bch.cloud.ms.servicios.previred.dtos.PreviredDTO;
import cl.bch.cloud.ms.servicios.previred.dtos.ConsumidorDTO;
import cl.bch.cloud.ms.servicios.previred.exceptions.PreviredException;
import cl.bch.cloud.ms.servicios.previred.properties.PreviredProperties;
import cl.bch.cloud.ms.servicios.previred.repositories.GetFolioDao;
import cl.bch.cloud.ms.servicios.previred.services.filtros.FiltroLlaveCompraStrategy;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class FiltroLlaveCompraStrategyTest {

    @Mock
    private PreviredProperties previredProperties;
    
    @Mock
    private GetFolioDao getFolioDao;

    @InjectMocks
    private FiltroLlaveCompraStrategy filtroLlaveCompraStrategy;


    @Test
    void APPLY_FILTER_LLAVE_COMPRA_TOC_TEST() {

        PreviredDTO previredDTO = new PreviredDTO();
        ConsumidorDTO consumidorDTO = new ConsumidorDTO();
        consumidorDTO.setNombre("TOC");
        consumidorDTO.setLlave("TOC-12345-13532256635");
        previredDTO.setInformacionConsumidor(consumidorDTO);

        filtroLlaveCompraStrategy.applyFilter(previredDTO);

    }

    @Test
    void APPLY_FILTER_LLAVE_COMPRA_EFX_TEST() {

        PreviredDTO previredDTO = new PreviredDTO();
        ConsumidorDTO consumidorDTO = new ConsumidorDTO();
        consumidorDTO.setNombre("EFX");
        consumidorDTO.setLlave("EFX-12345-13532256635");
        previredDTO.setInformacionConsumidor(consumidorDTO);

        filtroLlaveCompraStrategy.applyFilter(previredDTO);

    }

    @Test
    void APPLY_FILTER_LLAVE_COMPRA_VACIA_EFX_TEST() {

        PreviredDTO previredDTO = new PreviredDTO();
        ConsumidorDTO consumidorDTO = new ConsumidorDTO();
        consumidorDTO.setNombre("EFX");
        consumidorDTO.setLlave("");
        previredDTO.setUsernameSolicitante("gsuazoc");
        previredDTO.setInformacionConsumidor(consumidorDTO);

        Assertions.assertThrows(PreviredException.class, () -> {
            filtroLlaveCompraStrategy.applyFilter(previredDTO);
        });

    }


    @Test
    void APPLY_FILTER_LLAVE_COMPRA_BCH_TEST() {

        when(previredProperties.getNroCliente()).thenReturn("12345");
        when(previredProperties.getPrefijo()).thenReturn("BCH");

        PreviredDTO previredDTO = new PreviredDTO();
        ConsumidorDTO consumidorDTO = new ConsumidorDTO();
        consumidorDTO.setCanal("BCH");
        consumidorDTO.setLlave("123123");
        consumidorDTO.setNombre("BCH");
        consumidorDTO.setEmpresa("BCH");
        previredDTO.setUsernameSolicitante("gsuazoc");
        previredDTO.setRutClienteCompra("1-9");
        previredDTO.setInformacionConsumidor(consumidorDTO);
        
        when(getFolioDao.getFolio()).thenReturn(new BigDecimal("1"));

        filtroLlaveCompraStrategy.applyFilter(previredDTO);
    }

}
