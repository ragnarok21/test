package cl.bch.cloud.ms.servicios.previred.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;

import java.io.IOException;
import java.util.List;

public class JsonUtils {
    private JsonUtils() {
    }

    private static ObjectMapper getMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
        return mapper;
    }

    public static String convertObjectToJsonBytes(Object object) throws JsonProcessingException {
        ObjectMapper mapper = getMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        return mapper.writeValueAsString(object);
    }

    public static <T> T convertJsonToObject(String value, Class<T> type) throws IOException {
        ObjectMapper mapper = getMapper();
        return mapper.reader().forType(type).readValue(value);
    }

    public static <T> List<T> convertJsonToObjectList(String value, Class<T> listType) throws IOException {
        ObjectMapper mapper = getMapper();
        return (List)mapper.readValue(value, TypeFactory.defaultInstance().constructCollectionType(List.class, listType));
    }
}
