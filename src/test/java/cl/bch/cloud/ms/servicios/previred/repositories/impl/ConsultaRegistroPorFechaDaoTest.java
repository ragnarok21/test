package cl.bch.cloud.ms.servicios.previred.repositories.impl;


import cl.bch.cloud.ms.servicios.previred.exceptions.PreviredException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import java.math.BigDecimal;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ConsultaRegistroPorFechaDaoTest {

    @InjectMocks
    private ConsultaRegistroPorFechaDaoImpl repository;

    @Mock
    private SimpleJdbcCall simpleJdbcCall;

    @BeforeEach
    void before() {
        repository.setSimpleJdbcCall(this.simpleJdbcCall);
    }

    @Test
    void NO_EXISTE_REGISTRO_TEST() {

        when(repository
                .execute(any(Class.class), any(MapSqlParameterSource.class))).thenReturn(BigDecimal.ZERO);
        assertFalse(repository.existeRegistro(1, new Date(), new Date()));
    }

    @Test
    void EXISTE_REGISTRO_TEST() {

        when(repository
                .execute(any(Class.class), any(MapSqlParameterSource.class))).thenReturn(BigDecimal.ONE);
        assertTrue(repository.existeRegistro(1, new Date(), new Date()));
    }

    @Test
    void EXISTE_REGISTRO_BD_CONECTION_ERROR_TEST() {

        when(repository
                .execute(any(Class.class), any(MapSqlParameterSource.class))).thenThrow(mock(DataAccessException.class));
        assertThrows(PreviredException.class, () -> repository.existeRegistro(1, new Date(), new Date()));
    }
}
