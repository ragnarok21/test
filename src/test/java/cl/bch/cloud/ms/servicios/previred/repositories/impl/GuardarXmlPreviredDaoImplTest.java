package cl.bch.cloud.ms.servicios.previred.repositories.impl;

import cl.bch.cloud.ms.servicios.previred.dtos.ClienteCompraPreviredResponse;
import cl.bch.cloud.ms.servicios.previred.dtos.ConsumidorDTO;
import cl.bch.cloud.ms.servicios.previred.dtos.PreviredDTO;
import cl.bch.cloud.ms.servicios.previred.exceptions.PreviredException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.HashMap;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class GuardarXmlPreviredDaoImplTest {

    @Mock
    private SimpleJdbcCall simpleJdbcCall;

    @InjectMocks
    private GuardarXmlPreviredDaoImpl repository;

    private PreviredDTO previredDTO;

    private ClienteCompraPreviredResponse clienteCompraPreviredResponse;

    @BeforeEach
    void before() {
        previredDTO = new PreviredDTO();
        previredDTO.setIdCompra(BigDecimal.ONE);
        previredDTO.setRutClienteCompra("1-9");
        previredDTO.setUsernameSolicitante("gsuazoc");
        ConsumidorDTO consumidorDTO = new ConsumidorDTO();
        consumidorDTO.setNombre("BCH_Privado");
        consumidorDTO.setCanal("BCH_WEB");
        consumidorDTO.setLlave("");
        consumidorDTO.setRegistro("DDDDD");
        previredDTO.setInformacionConsumidor(consumidorDTO);
        clienteCompraPreviredResponse = new ClienteCompraPreviredResponse("1234", "");
        repository.setSimpleJdbcCall(this.simpleJdbcCall);
    }

    @Test
    void SAVE_XML_PREVIRED_TEST() {

        HashMap hashMap = new HashMap<>();
        hashMap.put("OUT_COD_ERROR", new BigDecimal(9000));
        hashMap.put("OUT_MENSAJE", "OK");
        when(repository.execute(any(MapSqlParameterSource.class))).thenReturn(hashMap);
        repository.saveXML(previredDTO, null, clienteCompraPreviredResponse);
    }

    @Test
    void SAVE_XML_PREVIRED_ERROR_SAVE_TEST() {
        HashMap hashMap = new HashMap<>();
        hashMap.put("OUT_COD_ERROR", new BigDecimal(9001));
        hashMap.put("OUT_MENSAJE", "Error al procesar xml previred");
        when(repository.execute(any(MapSqlParameterSource.class))).thenReturn(hashMap);

        Assertions.assertThrows(PreviredException.class, () -> {
            repository.saveXML(previredDTO, null, clienteCompraPreviredResponse);
        });
    }

    @Test
    void SAVE_XML_PREVIRED_DB_CONECTION_ERROR_TEST() {

        DataAccessException dataAccessException = Mockito.mock(DataAccessException.class);

        when(repository.execute(any(MapSqlParameterSource.class))).thenThrow(dataAccessException);

        Assertions.assertThrows(PreviredException.class, () -> {
            repository.saveXML(previredDTO, null, clienteCompraPreviredResponse);
        });
    }

}
