package cl.bch.cloud.ms.servicios.previred.services;

import cl.bch.cloud.ms.servicios.previred.dtos.ConsumidorDTO;
import cl.bch.cloud.ms.servicios.previred.dtos.PreviredDTO;
import cl.bch.cloud.ms.servicios.previred.exceptions.PreviredException;
import cl.bch.cloud.ms.servicios.previred.repositories.ConsultaRegistroUltimosMesesDao;
import cl.bch.cloud.ms.servicios.previred.restClients.PensionadoApiClient;
import cl.bch.cloud.ms.servicios.previred.services.filtros.FiltroCompraPensionadoStrategy;
import cl.bch.cloud.ms.servicios.previred.utils.FechaUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class FiltroCompraPensionadoStrategyTest {

	@InjectMocks
	private FiltroCompraPensionadoStrategy filtroCompraPensionadoStrategy;

	@Mock
	private ConsultaRegistroUltimosMesesDao dao;

	@Mock
	private FechaUtils fechaUtils;

	@Mock
	private PensionadoApiClient pensionadoApiClient;

	private Date localDateToDate(LocalDate localDate) {
		return Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
	}

	@Test
	void APPLY_FILTER_REGISTRO_SI_ULTIMOS_MESES_TEST() {

		PreviredDTO previredDTO = new PreviredDTO();
		ConsumidorDTO consumidorDTO = new ConsumidorDTO();
		consumidorDTO.setCanal("BCH");
		consumidorDTO.setLlave("123123");
		consumidorDTO.setNombre("BCH");
		consumidorDTO.setEmpresa("BCH");
		previredDTO.setUsernameSolicitante("gsuazoc");
		previredDTO.setRutClienteCompra("17208825-2");
		previredDTO.setInformacionConsumidor(consumidorDTO);
		previredDTO.setIdCompra(new BigDecimal("1"));
		previredDTO.setForzar(false);

		Map<String, Object> registro = new HashMap<>();
		ArrayList<Map<String, Object>> listaDetalles = new ArrayList<>();
		Map<String, Object> detalle = new HashMap<>();
		Date fechaBD = localDateToDate(LocalDate.of(2020, 7, 26));
		detalle.put("TIMESTAMP", fechaBD);
		detalle.put("CODIGO", "9360");
		listaDetalles.add(detalle);
		registro.put("OUT_DETALLES", listaDetalles);

		when(pensionadoApiClient.esPensionado(anyString())).thenReturn(false);

		when(fechaUtils.getDateNow()).thenReturn(LocalDate.of(2020, 7, 26));

		when(dao.obtenerUltimoRegistro(any())).thenReturn(registro);

		Assertions.assertThrows(PreviredException.class, () -> {
			filtroCompraPensionadoStrategy.applyFilter(previredDTO);
		});
	}
	
	@Test
	void APPLY_FILTER_REGISTRO_NO_ULTIMOS_MESES_TEST() {

		PreviredDTO previredDTO = new PreviredDTO();
		ConsumidorDTO consumidorDTO = new ConsumidorDTO();
		consumidorDTO.setCanal("BCH");
		consumidorDTO.setLlave("123123");
		consumidorDTO.setNombre("BCH");
		consumidorDTO.setEmpresa("BCH");
		previredDTO.setUsernameSolicitante("gsuazoc");
		previredDTO.setRutClienteCompra("17208825-2");
		previredDTO.setInformacionConsumidor(consumidorDTO);
		previredDTO.setIdCompra(new BigDecimal("1"));
		previredDTO.setForzar(false);

		Map<String, Object> registro = new HashMap<>();
		ArrayList<Map<String, Object>> listaDetalles = new ArrayList<>();
		Map<String, Object> detalle = new HashMap<>();
		Date fechaBD = localDateToDate(LocalDate.of(2020, 1, 26));
		detalle.put("TIMESTAMP", fechaBD);
		detalle.put("CODIGO", "9365");
		listaDetalles.add(detalle);
		registro.put("OUT_DETALLES", listaDetalles);

		when(pensionadoApiClient.esPensionado(anyString())).thenReturn(false);

		when(fechaUtils.getDateNow()).thenReturn(LocalDate.of(2020, 7, 26));

		when(dao.obtenerUltimoRegistro(any())).thenReturn(registro);

		filtroCompraPensionadoStrategy.applyFilter(previredDTO);
	}
	

	@Test
	void APPLY_FILTER_COMPRA_PENSIONADO_TEST() {

		PreviredDTO previredDTO = new PreviredDTO();
		ConsumidorDTO consumidorDTO = new ConsumidorDTO();
		consumidorDTO.setCanal("BCH");
		consumidorDTO.setLlave("123123");
		consumidorDTO.setNombre("BCH");
		consumidorDTO.setEmpresa("BCH");
		previredDTO.setUsernameSolicitante("gsuazoc");
		previredDTO.setRutClienteCompra("1-9");
		previredDTO.setInformacionConsumidor(consumidorDTO);
		previredDTO.setIdCompra(new BigDecimal("1"));
		previredDTO.setForzar(false);

		when(pensionadoApiClient.esPensionado(anyString())).thenReturn(true);

		Assertions.assertThrows(PreviredException.class, () -> {
			filtroCompraPensionadoStrategy.applyFilter(previredDTO);
		});
	}

}
