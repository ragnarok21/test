package cl.bch.cloud.ms.servicios.previred.repositories.impl;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import cl.bch.cloud.ms.servicios.previred.exceptions.PreviredException;

@ExtendWith(MockitoExtension.class)
public class ConsultaRegistroUltimosMesesDaoImplTest {

	@InjectMocks
	ConsultaRegistroUltimosMesesDaoImpl repository;

	@Mock
	private SimpleJdbcCall simpleJdbcCall;

	@BeforeEach
	void before() {
		repository.setSimpleJdbcCall(this.simpleJdbcCall);
	}

	@Test
	public void EXISTE_REGISTRO_TEST() {

		when(simpleJdbcCall.execute(any(MapSqlParameterSource.class))).thenReturn(any(Map.class));
		repository.obtenerUltimoRegistro("172088251");
	}

	@Test
	public void NO_EXISTE_REGISTRO_TEST() {

		DataAccessException dataAccessException = Mockito.mock(DataAccessException.class);

		when(simpleJdbcCall.execute(any(MapSqlParameterSource.class))).thenThrow(dataAccessException);
		assertThrows(PreviredException.class, () -> repository.obtenerUltimoRegistro("172088252"));

	}

}
