package cl.bch.cloud.ms.servicios.previred.utils;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CleanUtilsTest {

    @Test
    void cleanRutSinGuionPuntosTest() {

        assertEquals("122323623", CleanUtils.cleanRutSinGuionPuntos("12.232.362-3"));
    }

    @Test
    void removeNewLinesEmptyStringTest() {

        assertEquals("", CleanUtils.clean(""));
    }
}