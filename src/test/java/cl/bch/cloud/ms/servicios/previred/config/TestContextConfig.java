package cl.bch.cloud.ms.servicios.previred.config;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
@ComponentScan(basePackages = {"cl.bch.cloud.ms.servicios.previred", "cl.bancochile.ws.client.previred"})
public class TestContextConfig {


}
