package cl.bch.cloud.ms.servicios.previred.services;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import cl.bch.cloud.ms.servicios.previred.dtos.ConsumidorDTO;
import cl.bch.cloud.ms.servicios.previred.dtos.PreviredDTO;
import cl.bch.cloud.ms.servicios.previred.exceptions.PreviredException;
import cl.bch.cloud.ms.servicios.previred.repositories.ValidaUsuarioDao;
import cl.bch.cloud.ms.servicios.previred.services.filtros.FiltroCompraPermitidaStrategy;

@ExtendWith(MockitoExtension.class)
public class FiltroCompraPermitidaStrategyTest {

	@InjectMocks
	private FiltroCompraPermitidaStrategy filtroCompraPermitidaStrategy;

	@Mock
	private ValidaUsuarioDao validaUsuarioDao;

	private PreviredDTO previredDTO = new PreviredDTO();

	@BeforeEach
	void init() {

		ConsumidorDTO consumidorDTO = new ConsumidorDTO();
		consumidorDTO.setCanal("BCH");
		consumidorDTO.setLlave("123123");
		consumidorDTO.setNombre("BCH");
		consumidorDTO.setRegistro("12lklfdkqwerta123asd==");
		previredDTO.setUsernameSolicitante("gsuazoc");
		previredDTO.setRutClienteCompra("1-9");
		previredDTO.setInformacionConsumidor(consumidorDTO);
		previredDTO.setIdCompra(new BigDecimal(1));
		previredDTO.setForzar(true);
	}

	@Test
	void APPLY_FILTER_LLAVE_COMPRA_PERMITIDA_TEST_ERROR_GENERAL() {

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("OUT_COD_ERROR", BigDecimal.valueOf(0));

		when(validaUsuarioDao.validaUsuario(any())).thenReturn(map);

		filtroCompraPermitidaStrategy.applyFilter(previredDTO);
	}

	@Test
	void APPLY_FILTER_LLAVE_COMPRA_PERMITIDA_TEST_ERROR_PREVIRED() {

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("OUT_COD_ERROR", BigDecimal.valueOf(100));

		when(validaUsuarioDao.validaUsuario(any())).thenReturn(map);

		Assertions.assertThrows(PreviredException.class, () -> filtroCompraPermitidaStrategy.applyFilter(previredDTO));
	}

	@Test
	void APPLY_FILTER_LLAVE_COMPRA_PERMITIDA_TEST_ERROR_PREVIRED_GENERAL() {

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("OUT_COD_ERROR", BigDecimal.valueOf(200));

		when(validaUsuarioDao.validaUsuario(any())).thenReturn(map);

		Assertions.assertThrows(PreviredException.class, () -> filtroCompraPermitidaStrategy.applyFilter(previredDTO));
	}

}
