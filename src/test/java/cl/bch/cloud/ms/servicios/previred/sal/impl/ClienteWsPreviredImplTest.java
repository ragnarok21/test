package cl.bch.cloud.ms.servicios.previred.sal.impl;

import cl.bancochile.ws.client.previred.ClienteWsCompraPreviredService;
import cl.bancochile.ws.client.previred.domain.response.Control;
import cl.bancochile.ws.client.previred.domain.response.Respuesta;
import cl.bancochile.ws.client.previred.domain.response.RespuestaServicio;
import cl.bch.cloud.ms.servicios.previred.dtos.ConsumidorDTO;
import cl.bch.cloud.ms.servicios.previred.dtos.PreviredDTO;
import cl.bch.cloud.ms.servicios.previred.exceptions.ClientePreviredException;
import cl.bch.cloud.ms.servicios.previred.exceptions.PreviredException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.xml.bind.JAXBException;
import java.rmi.RemoteException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ClienteWsPreviredImplTest {

    @Mock
    private ClienteWsCompraPreviredService wsCompraPreviredService;

    @InjectMocks
    private ClienteWsPreviredImpl clienteWsPrevired;

    @Test
    void REMOTE_ACCESS_EXCEPTION_CALL_WS_TEST() throws Exception {

        PreviredDTO previredDTO = new PreviredDTO();
        previredDTO.setRutClienteCompra("1-9");
        ConsumidorDTO consumidorDTO = new ConsumidorDTO();
        consumidorDTO.setNombre("EFX");
        consumidorDTO.setLlave("");
        previredDTO.setInformacionConsumidor(consumidorDTO);

        doThrow(RemoteException.class).when(wsCompraPreviredService)
                .obtenerCotizacionesPrevired(any());

        Assertions.assertThrows(PreviredException.class, () -> clienteWsPrevired.callWs(previredDTO, new Timestamp(1234)));
    }

    @Test
    void JAXB_EXCEPTION_CALL_WS_TEST() throws Exception {

        PreviredDTO previredDTO = new PreviredDTO();
        previredDTO.setRutClienteCompra("1-9");
        ConsumidorDTO consumidorDTO = new ConsumidorDTO();
        consumidorDTO.setNombre("EFX");
        consumidorDTO.setLlave("");
        previredDTO.setInformacionConsumidor(consumidorDTO);

        doThrow(JAXBException.class).when(wsCompraPreviredService)
                .obtenerCotizacionesPrevired(any());

        Assertions.assertThrows(PreviredException.class, () -> clienteWsPrevired.callWs(previredDTO, new Timestamp(1234)));
    }

    @Test
    void SUCCESS_CALL_WS_TEST() throws Exception {

        PreviredDTO previredDTO = new PreviredDTO();
        previredDTO.setRutClienteCompra("1-9");
        ConsumidorDTO consumidorDTO = new ConsumidorDTO();
        consumidorDTO.setNombre("EFX");
        consumidorDTO.setLlave("");
        previredDTO.setInformacionConsumidor(consumidorDTO);

        Respuesta respuesta = new Respuesta();
        Control control = new Control();
        control.setCodigo("9000");
        respuesta.setControl(control);


        when(wsCompraPreviredService
                .obtenerCotizacionesPrevired(any())).thenReturn(respuesta);


        assertEquals("9000", clienteWsPrevired.callWs(previredDTO, new Timestamp(1234)).getCodigo());
    }
    
}