package cl.bch.cloud.ms.servicios.previred.services;

import cl.bch.cloud.ms.servicios.previred.dtos.PreviredDTO;
import cl.bch.cloud.ms.servicios.previred.exceptions.PreviredException;
import cl.bch.cloud.ms.servicios.previred.repositories.ConsultaRegistroPorFechaDao;
import cl.bch.cloud.ms.servicios.previred.services.filtros.FiltroCompraDiariaStrategy;
import cl.bch.cloud.ms.servicios.previred.utils.FechaUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.Date;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class FiltroCompraDiariaStrategyTest {
    
    @Mock
    private ConsultaRegistroPorFechaDao consultaRegistroPorFechaDao;
    
    @InjectMocks
    private FiltroCompraDiariaStrategy filtroCompraDiariaStrategy;

    @Mock
    private FechaUtils fechaUtils;

    @BeforeEach
    void init() {
        LocalDate now = LocalDate.of(2020, 7, 13);
        when(fechaUtils.getDateNow()).thenReturn(now);

        when(fechaUtils.localDateToDate(any(LocalDate.class))).thenCallRealMethod();

    }

    @Test
    void APPLY_FILTER_FILTRO_COMPRA_DIARIA_TEST() {

        PreviredDTO previredDTO = new PreviredDTO();
        previredDTO.setRutClienteCompra("1-9");

        Mockito.when(consultaRegistroPorFechaDao.existeRegistro(
                Mockito.any(Integer.class),
                Mockito.any(Date.class),
                Mockito.any(Date.class))).thenReturn(Boolean.FALSE);

        filtroCompraDiariaStrategy.applyFilter(previredDTO);

    }
    
    @Test
    void APPLY_FILTER_FILTRO_COMPRA_DIARIA_REGISTRO_EXISTENTE_TEST() {

        PreviredDTO previredDTO = new PreviredDTO();
        previredDTO.setRutClienteCompra("1-9");

        Mockito.when(consultaRegistroPorFechaDao.existeRegistro(
                Mockito.any(Integer.class),
                Mockito.any(Date.class),
                Mockito.any(Date.class))).thenReturn(Boolean.TRUE);


        Assertions.assertThrows(PreviredException.class, () -> {
            filtroCompraDiariaStrategy.applyFilter(previredDTO);
        });
    }
}
